#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Chainsaw
#
# Copyright (C) 2021 Grégory David <dev@groolot.net>
#
# Chainsaw is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Chainsaw is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

"""Allow user to run Chainsaw as a module."""

# Execute with:
# $ python3 -m chainsaw

import chainsaw

if __name__ == '__main__':
    chainsaw.main()
